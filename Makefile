BIN=./bin
CC=gcc

$(BIN)/Day1: Day1/main.c
	$(CC) Day1/main.c -o $(BIN)/Day1

Day1: $(BIN)/Day1
	$(BIN)/Day1 Day1/input.dat

clean:
	rm -rf $(BIN)/*
