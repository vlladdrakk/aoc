#include <stdio.h>

int main(int argc, char** argv) {
  if (argc != 2) {
    fprintf(stderr, "Insufficient arguments\n");
  }
  char* filename = argv[1];
  FILE* file = fopen(filename, "r");

  char sign;
  int val;
  int running_total = 0;
  while ((fscanf(file, "%d", &val)) == 1) {
    running_total += val;
  }

  printf("%d\n", running_total);

  return 0;
}
