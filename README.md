# AOC

My Advent Of Code solutions

## Usage
To compile all of the challenges just run make like usual.<br/>
To compile and run a specific day's challenge run: `make DayX` where X is the day number.